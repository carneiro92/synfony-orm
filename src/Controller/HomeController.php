<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PersonType;
use App\Entity\Person;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\PersonRepository;
use Doctrine\Common\Persistence\ObjectManager;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(PersonRepository $personRepository)
    {
        $person=$personRepository->findAll();
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'person'=>$person
        ]);
    }
    /**
     * @Route("/add", name="add")
     */
    public function addForm(PersonType $form,PersonRepository $repo,Request $request, ObjectManager $objectManager)
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            dump($person);
            $objectManager->persist($person);
            $objectManager->flush();
        }

        return $this->render("home/add-person.html.twig", [
            "form" => $form->createView()
        ]);
    }
}
